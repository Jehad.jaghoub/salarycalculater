<?php

namespace App\Http\Controllers;

use App\userField;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserFeild;

class FiledController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Fields = \App\userField::Gorder()->get();
        return view("Fileds.index", compact('Fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Fileds.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserFeild $request)
    {
        $validated = $request->validated();
        \App\userField::Create([
          'name'=>$validated['FeildName'],
          'order'=>$validated['FeildOrder'],
          'is_checkbox'=>(isset($validated['is_checkbox']) && $validated['is_checkbox']) ? 1 : 0
        ]);
        return redirect(route('Fileds.index'))->with('success', __("Field Added successfully"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userField  $userField
     * @return \Illuminate\Http\Response
     */
    public function show(userField $userField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userField  $userField
     * @return \Illuminate\Http\Response
     */
    public function edit($id, userField $userField)
    {
        $Field = $userField->findOrFail($id);
        return view('Fileds.edit', compact('Field'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userField  $userField
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreUserFeild $request, userField $userField)
    {
        $Field = $userField->findOrFail($id);
        $validated = $request->validated();
        $Field->name = $validated['FeildName'];
        $Field->order = $validated['FeildOrder'];
        $Field->is_checkbox = (isset($validated['is_checkbox']) && $validated['is_checkbox']) ? 1 : 0;
        $Field->save();
        return redirect(route('Fileds.index'))->with('success', __("Field Edited successfully"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userField  $userField
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, userField $userField)
    {
        $Field = $userField->findOrFail($id);
        $Field->delete();
        return redirect(route('Fileds.index'))->with('success', __("Field Deleted successfully"));
    }
}
