<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $Fields = \App\userField::Gorder()->get();
        $TotallogCount = \App\Userlog::count();
        $Recents =  \App\Userlog::orderBy('created_at', 'desc')->limit(5)->get();
        $AvgNet = \App\Userlog::avg('result');
        $avgTax = \App\Userlog::avg('Country_Tax');
        $AvgSalary = \App\Userlog::avg('Employee_Salary');
        $AvgNet= money_format('$%i', $AvgNet);
        $AvgSalary= money_format('$%i', $AvgSalary);
        return view('index', compact('Fields', 'Recents', 'TotallogCount', 'AvgNet', 'AvgSalary', 'avgTax'));
    }



    /**
    * Calculate the salary for user
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function Calculate(Request $Request)
    {
        $input = $Request->all();
        $Name = $input['EmployeeName'];
        $GrossSalary =floatval($input['Employee_Salary']);
        $CountryTax = floatval($input['Country_Tax']);
        $TaxExtra = 0;
        $TotalTax = 0 ;
        $NotEffectedWithTax = 0;
        foreach ($input as $key => $value) {
            // get automatic field
            $FieldName = explode("_", $key);
            if ($FieldName[0]=="filedCalc") {
                // check if automatic field is exist
                if (isset($FieldName[1])) {
                    $FieldID = $FieldName[1];
                    // get the field toghther with the roles
                    $TheFeild=  \App\userField::with("roles")->findOrFail($FieldID);
                    // get the roles to fetch it
                    $Roles = $TheFeild->roles;
                    foreach ($Roles as $role) {
                        $isActive = false;
                        if ($TheFeild->is_checkbox) {
                            // if its  a check box no need for roles meet on amount and meet on
                            if ($value>0) {
                                $isActive = true;
                            }
                        } else {
                            switch ($role->role_meet_on) {
                              case 0:
                               $isActive = $value > $role->role_on_meet_amount;
                              break;
                              case 1:
                              $isActive = $value < $role->role_on_meet_amount;
                              break;
                              case 2:
                              $isActive = $value === $role->role_on_meet_amount;
                              break;
                          }
                        }

                        if ($isActive) {
                            if ($role->will_effect>0) {
                                // it will effect Tax
                                // here we need to viseversa as bounes on Tax mean minus on percent
                                if ($role->effect_type>0) {
                                    $mulsign = 1;
                                } else {
                                    $mulsign = -1;
                                }
                                // calculate Tax
                                if ($role->effect_is_precent>0) {
                                    $CountryTax = $CountryTax + ($mulsign*$role->effect_amount);
                                } else {
                                    $TotalTax += ($mulsign*$role->effect_amount);
                                }
                            } else {
                                // it will effect Salary
                                if ($role->effect_type>0) {
                                    $mulsign = -1;
                                    if ($role->effect_is_precent>0) {
                                        $Amount = ($GrossSalary*$role->effect_amount)/100;
                                        $NotEffectedWithTax += ($mulsign*$Amount);
                                    } else {
                                        $NotEffectedWithTax += ($mulsign*$role->effect_amount);
                                    }
                                } else {
                                    $mulsign = 1;
                                    if ($role->effect_is_precent>0) {
                                        $Amount = ($GrossSalary*$role->effect_amount)/100;
                                        $GrossSalary +=  ($mulsign*$Amount);
                                    } else {
                                        $GrossSalary +=  ($mulsign*$role->effect_amount);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $TaxAmount = (($GrossSalary*$CountryTax)/100)+$TotalTax;
        $NetSalary = $GrossSalary  - $TaxAmount ;
        $NetSalary+= $NotEffectedWithTax;
        $Fromated = money_format('$%i', $NetSalary);
        \App\Userlog::Create([
          'Employee_Salary'=>$input['Employee_Salary'],
          'Country_Tax'=>$input['Country_Tax'],
          'result'=>$NetSalary,
          'data'=>json_encode($input),
        ]);
        $Fields = \App\userField::Gorder()->get();
        $TotallogCount = \App\Userlog::count();
        $Recents =  \App\Userlog::orderBy('created_at', 'desc')->limit(5)->get();
        $AvgNet = \App\Userlog::avg('result');
        $avgTax = \App\Userlog::avg('Country_Tax');
        $AvgSalary = \App\Userlog::avg('Employee_Salary');
        $AvgNet= money_format('$%i', $AvgNet);
        $AvgSalary= money_format('$%i', $AvgSalary);
        return view('index', compact('Fields', 'Recents', 'TotallogCount', 'AvgNet', 'AvgSalary', 'avgTax', 'Fromated'));
    }
}
