<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRole;

class RoleController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Roles = \App\Role::with("Field")->get();
        return view("Roles.index", compact('Roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $FieldGet = \Request::get('Field');
        $Fields = \App\userField::Gorder()->get();
        return view("Roles.create", compact('FieldGet', 'Fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRole $request)
    {
        $validated = $request->validated();
        Role::Create([
          "name"                => $validated['RoleName'],
          "userfield_id"        => $validated['userfield_id'],
          "will_effect"         => $validated['will_effect'],
          "effect_type"         => $validated['effect_type'],
          "effect_amount"       => $validated['effect_amount'],
          "effect_is_precent"   => (isset($validated['effect_is_precent']) && $validated['effect_is_precent']) ? 1 : 0,
          "role_meet_on"        => $validated['role_meet_on'],
          "role_on_meet_amount" => $validated['role_on_meet_amount']
        ]);
        return redirect(route('Roles.index'))->with('success', __("Role Added successfully"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Role $role)
    {
        $Role = $role->findOrFail($id);
        $Fields = \App\userField::Gorder()->get();
        return view('Roles.edit', compact('Role', 'Fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreRole $request)
    {
        $RoleData = Role::findOrFail($id);
        $validated = $request->validated();

        $RoleData->name = $validated['RoleName'];
        $RoleData->userfield_id = $validated['userfield_id'];
        $RoleData->will_effect = $validated['will_effect'];
        $RoleData->effect_type = $validated['effect_type'];
        $RoleData->effect_amount = $validated['effect_amount'];
        $RoleData->effect_is_precent = (isset($validated['effect_is_precent']) && $validated['effect_is_precent']) ? 1 : 0;
        $RoleData->role_meet_on = $validated['role_meet_on'];
        $RoleData->role_on_meet_amount = $validated['role_on_meet_amount'];
        $RoleData->save();
        return redirect(route('Roles.index'))->with('success', __("Role Edited successfully"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Role $role)
    {
        $role = $role->findOrFail($id);
        $role->delete();
        return redirect(route('Roles.index'))->with('success', __("Role Deleted successfully"));
    }
}
