<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRole extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'RoleName'=>'required|min:3',
            'userfield_id'=>'required|exists:user_fields,id',
            'will_effect'=>'required|boolean',
            'effect_type'=>'required|boolean',
            'effect_amount'=>'required|integer',
            'effect_is_precent'=>'sometimes|required|accepted',
            'role_meet_on'=>'nullable|in:0,1,2',
            'role_on_meet_amount'=>'nullable|integer'
        ];
    }
}
