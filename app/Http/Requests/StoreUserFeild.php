<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserFeild extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'FeildName'=>'required|min:3',
            'FeildOrder'=>'required|integer',
            'is_checkbox'=>'sometimes|required|accepted'
        ];
    }
}
