<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [''];

    public function Field()
    {
        return $this->belongsTo('App\userField', 'userfield_id', 'id');
    }
}
