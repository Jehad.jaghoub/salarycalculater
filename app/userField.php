<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userField extends Model
{
    protected $table = 'user_fields';
    protected $guarded = [''];
    public function scopeGorder($query)
    {
        global $table;
        return $query->orderBy($table.'order');
    }

    public function roles()
    {
        return $this->hasMany('App\Role', 'userfield_id', 'id');
    }
}
