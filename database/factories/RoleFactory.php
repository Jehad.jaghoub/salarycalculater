<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Role;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Role::class, function (Faker $faker) {
    return [
      'name'=>$faker->name,
      'userfield_id'=> $faker->numberBetween(1, 10),
      'will_effect'=>$faker->boolean(25),
      'effect_type'=>$faker->boolean(25),
      'effect_amount'=>$faker->numberBetween(1, 10000000),
      'effect_is_precent'=>$faker->boolean(25),
      'role_meet_on'=>$faker->numberBetween(0, 3),
      'role_on_meet_amount'=>$faker->numberBetween(1, 10000000)
    ];
});
