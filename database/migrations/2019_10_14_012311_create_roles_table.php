<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('userfield_id');
            $table->boolean('will_effect');
            $table->integer('effect_type');
            $table->integer('effect_amount');
            $table->boolean('effect_is_precent');
            $table->integer('role_meet_on')->nullable();
            $table->integer('role_on_meet_amount')->nullable();
            $table->foreign('userfield_id')->references('id')
            ->on('user_fields')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
