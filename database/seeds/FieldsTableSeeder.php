<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_fields')->insert([
          'name'=>'Age',
          'order'=>1,
          'is_checkbox'=>0,
       ]);
        DB::table('user_fields')->insert([
         'name'=>'Number of Kids',
         'order'=>1,
         'is_checkbox'=>0,
      ]);
        DB::table('user_fields')->insert([
          'name'=>'Use Company Car',
          'order'=>1,
          'is_checkbox'=>1,
     ]);
    }
}
