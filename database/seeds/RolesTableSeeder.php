<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
          'name'=>'Age More Than',
          'userfield_id'=>'1',
          'will_effect'=>'0',
          'effect_type'=>'0',
          'effect_amount'=>'7',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'50'
       ]);
        DB::table('roles')->insert([
          'name'=>'Kids More than',
          'userfield_id'=>'2',
          'will_effect'=>'1',
          'effect_type'=>'0',
          'effect_amount'=>'2',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'2'
      ]);
        DB::table('roles')->insert([
          'name'=>'Use Company Car',
          'userfield_id'=>'3',
          'will_effect'=>'0',
          'effect_type'=>'1',
          'effect_amount'=>'500',
          'effect_is_precent'=>'0',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'0'
     ]);
    }
}
