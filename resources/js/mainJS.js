$(document).ready(function() {
    // if first value of Fields is a check box remove roles values
    if ($("#NotCheckedBox").length) {
        is_checkbox = $("#userfield_id").find(":selected").data('checkbox');
        hideifcheckbox(is_checkbox)
    }

    // form validation
    $("#SubmitForm").on("click", function(e) {
        var form = $(this).parents('form');
        if (form[0].checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
        }
        form.addClass('was-validated');
    });
    // delete element confirm
    $(".DeleteElement").on("click", function(e) {
        e.preventDefault();
        var name = $(this).data('name');
        var id = $(this).data('id');
        Swal.fire({
            title: 'do you want to delete?',
            text: name,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $("#DeleteElement" + id).submit();
            }
        })
    })
    // hide or display roles value according to selected field
    $("#userfield_id").on("change", function() {
        var is_checkbox = $(this).find(":selected").data('checkbox');
        hideifcheckbox(is_checkbox)
    });

});


function hideifcheckbox(is_checkbox) {
    if (is_checkbox) {
        $("#NotCheckedBox").fadeOut();
        $("#role_meet_on").attr('required', false);
        $("#role_on_meet_amount").attr('required', false);

    } else {
        $("#NotCheckedBox").fadeIn();
        $("#role_meet_on").attr('required', true);
        $("#role_on_meet_amount").attr('required', true);
    }
}