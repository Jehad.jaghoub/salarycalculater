@extends('layouts.mainLayout')
@section('content')

<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">{{__("Add User Fields")}}</h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{__("Add User Field Form")}}</h6>
    </div>
    <div class="card-body">
  <form  action="{{route('Fileds.store')}}" method="post">
    @csrf


   <div class="form-group">
     <label for="FeildName">{{__("Feild Name")}}</label>
     <input type="text" class="form-control @error('FeildName') is-invalid @enderror" name="FeildName" id="FeildName" aria-describedby="FeildName" required minlength="3" placeholder="{{__("eg. age")}}">
     @error('FeildName')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <small id="FeildHelp" class="form-text text-muted">{{__("to be displayed for users.")}}</small>
     <div class="invalid-feedback">
      {{__("it's a required Field and shall be over 3 letters")}}
     </div>
   </div>
   <div class="form-group">
     <label for="FeildOrder">{{__("Order")}}</label>
     <select required class="form-control @error('FeildOrder') is-invalid @enderror" name="FeildOrder" id="FeildOrder">
       @for ($i = 1; $i < 11; $i++)
         <option value="{{$i}}">{{$i}}</option>
        @endfor
     </select>
     @error('FeildOrder')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
      <div class="invalid-feedback">
        {{__("it's a required Field")}}
      </div>
   </div>
   <div class="form-group">
    <div class="custom-control custom-switch">
     <input type="checkbox" value="1" name="is_checkbox" class="custom-control-input" id="is_checkbox">
     <label class="custom-control-label" for="is_checkbox">{{__("Field is a Checkbox?")}}</label>
    </div>
  </div>

   <button id="SubmitForm" type="submit" class="btn btn-success btn-icon-split">
     <span class="icon text-white-50">
       <i class="fas fa-check"></i>
     </span>
      <span class="text">{{__("Submit")}}</span>
    </button>
 </form>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection
