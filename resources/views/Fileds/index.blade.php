@extends('layouts.mainLayout')
@section('content')

<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">{{("User Fields")}}</h1>
  <p class="mb-4">
    {{__("User Fields: this is the factor that will appear to users to calculate their salary according to it")}} <b>{{__(" eg. Age")}}</b>
     <a target="_blank" href="/help"> {{__("Visit help for more info !")}} </a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{("User Fields Table")}}</h6>
    </div>
    <div class="card-body">
      @if(count($Fields))
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>{{__("Order")}}</th>
              <th>{{__("Name")}}</th>
              <th>{{__("Is a checkbox ?")}}</th>
              <th>{{__("Actions")}}</th>
            </tr>
          </thead>
          <tbody>
          @foreach($Fields as $Field)
            <tr>
              <td>{{$Field->order}}</td>
              <td>{{$Field->name}}</td>
              <td>{{$Field->is_checkbox ? __("Yes") : __("No") }}</td>
              <td>
                <a href="{{route('Fileds.edit',['Filed' => $Field->id])}}" class="btn btn-secondary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-pen"></i>
                  </span>
                  <span class="text">{{__("Edit Feild")}}</span>
                </a>
                <a href="#" data-id="{{$Field->id}}" data-name="{{$Field->name}}" class="btn DeleteElement btn-danger btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-trash"></i>
                  </span>
                  <span class="text">{{__("Delete Feild")}}</span>
                </a>
                  <form style="display:inline;" id="DeleteElement{{$Field->id}}" action="{{route('Fileds.destroy',['Filed' => $Field->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                </form>

                <a href="{{route('Roles.create')}}?Field={{$Field->id}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">{{__("Add Rule")}}</span>
                  </a>

              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      @else
      {{__("there is not any fields yet please click add a field to add them.")}}
      @endif
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection
