@extends('layouts.mainLayout')
@section('content')

<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">{{__("Add Role")}}</h1>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{__("Add Role Form")}}</h6>
    </div>
    <div class="card-body">
  <form  action="{{route('Roles.store')}}" method="post">
    @csrf


   <div class="form-group">
     <label for="RoleName">{{__("Role Name")}}</label>
     <input type="text" class="form-control @error('RoleName') is-invalid @enderror" name="RoleName" id="RoleName" aria-describedby="RoleName" required minlength="3" placeholder="{{__("eg. age more than")}}">
     @error('RoleName')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <small id="FeildHelp" class="form-text text-muted">{{__("it will show only for you")}}</small>
     <div class="invalid-feedback">
      {{__("it's a required Field and shall be over 3 letters")}}
     </div>
   </div>



   <div class="form-group">
     <label for="userfield_id">{{__("Effected Field")}}</label>
     <select required class="form-control @error('userfield_id') is-invalid @enderror" name="userfield_id" id="userfield_id">
       @foreach($Fields as $Field)
         <option @if($FieldGet==$Field->id) selected @endif data-checkbox="{{$Field->is_checkbox}}" value="{{$Field->id}}">{{$Field->name}}</option>
       @endforeach
     </select>
     @error('userfield_id')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
      <div class="invalid-feedback">
        {{__("it's a required Field")}}
      </div>
   </div>

<div class="row">
   <div class="form-group col-6">
     <label for="will_effect">{{__("it Will Effect")}}</label>
     <select required class="form-control @error('will_effect') is-invalid @enderror" name="will_effect" id="will_effect">
          <option  value="0">{{__("Salary")}}</option>
          <option  value="1">{{__("Tax")}}</option>
      </select>
     @error('will_effect')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
      <div class="invalid-feedback">
        {{__("it's a required Field")}}
      </div>
   </div>

   <div class="form-group col-6">
     <label for="effect_type">{{__("Type of Effect")}}</label>
     <select required class="form-control @error('effect_type') is-invalid @enderror" name="effect_type" id="effect_type">
          <option  value="0">{{__("bonuses")}}</option>
          <option  value="1">{{__("deductions")}}</option>
      </select>
     @error('effect_type')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
      <div class="invalid-feedback">
        {{__("it's a required Field")}}
      </div>
   </div>
 </div>
 <div class="row">
   <div class="form-group col-4">
     <label for="effect_amount">{{__("Effect Amount")}}</label>
     <input type="number" min="1"  class="form-control @error('effect_amount') is-invalid @enderror" name="effect_amount" id="effect_amount" aria-describedby="RoleName" required  placeholder="{{__("eg. 10")}}">
     @error('effect_amount')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <div class="invalid-feedback">
      {{__("it's a required Field and shall be number bigger than 1")}}
     </div>
   </div>
   <div style="margin-top:38px;" class="form-group col-8">
    <div class="custom-control custom-switch">
     <input type="checkbox" value="1" name="effect_is_precent" class="custom-control-input" id="effect_is_precent">
     <label class="custom-control-label" for="effect_is_precent">{{__("Effect is on precent base?")}}</label>
    </div>
  </div>
</div>


<div id="NotCheckedBox" class="row">
   <div class="form-group col-6">
     <label for="role_meet_on">{{__("Rule Will Applied on")}}</label>
     <select required class="form-control @error('role_meet_on') is-invalid @enderror" name="role_meet_on" id="role_meet_on">
          <option  value="0">{{__("more than")}}</option>
          <option  value="1">{{__("less than")}}</option>
          <option  value="2">{{__("is equal")}}</option>
    </select>
     @error('role_meet_on')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
      <div class="invalid-feedback">
        {{__("it's a required Field")}}
      </div>
   </div>

   <div class="form-group col-6">
     <label for="role_on_meet_amount">{{__("Role Value")}}</label>
     <input type="number" min="1" class="form-control @error('role_on_meet_amount') is-invalid @enderror" name="role_on_meet_amount" id="role_on_meet_amount" aria-describedby="role_on_meet_amount" required  placeholder="{{__("eg. 50")}}">
     @error('role_on_meet_amount')
         <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <div class="invalid-feedback">
      {{__("it's a required Field and shall be number bigger than 1")}}
     </div>
 </div>
</div>

   <button id="SubmitForm" type="submit" class="btn btn-success btn-icon-split">
     <span class="icon text-white-50">
       <i class="fas fa-check"></i>
     </span>
      <span class="text">{{__("Submit")}}</span>
    </button>
 </form>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection
