@extends('layouts.mainLayout')
@section('content')

<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">{{("Roles for User Fields")}}</h1>
  <p class="mb-4">
    {{__("Roles: this will make the Calculation roles how we will calculate the salary according to user input compare to these role")}} <b>{{__(" eg. if Age > 50 add 7% to his salary")}}</b>
     <a target="_blank" href="/help"> {{__("Visit help for more info !")}} </a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{("Roles Table")}}</h6>
    </div>
    <div class="card-body">
      @if(count($Roles))
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>{{__("ID")}}</th>
              <th>{{__("Name")}}</th>
              <th>{{__("Belongs to")}}</th>
              <th>{{__("sammary")}}</th>
              <th>{{__("Actions")}}</th>
            </tr>
          </thead>
          <tbody>
          @foreach($Roles as $Role)
            <tr>
              <td>{{$Role->id}}</td>
              <td>{{$Role->name}}</td>
              <td>{{$Role->Field->name}}</td>
              @if($Role->Field->is_checkbox)
              <td>{{__("if")}} {{$Role->Field->name}} {{__("is checked")}} then<br>
                @if($Role->will_effect>0)
                {{__("Tax")}}
                @else
                {{__("Salary")}}
                @endif
                will be
                @if($Role->effect_type>0)
                {{__("deductions")}}
                @else
                {{__("bonuses")}}
                @endif
                <br>
                <b>{{$Role->effect_amount}}
                  @if($Role->effect_is_precent>0)
                  %
                  @else
                  $
                  @endif
                </b>
               </td>
              @else
              <td>{{__("if")}} {{$Role->Field->name}}
                @switch($Role->role_meet_on)
                @case(0)
                {{__("more than")}}
                @break;
                @case(1)
                {{__("less than")}}
                @break;
                @case(2)
                {{__("is equal")}}
                @break;
                @endswitch
                {{$Role->role_on_meet_amount}}
                <br>
                then
                @if($Role->will_effect>0)
                {{__("Tax")}}
                @else
                {{__("Salary")}}
                @endif
                will be
                @if($Role->effect_type>0)
                {{__("deductions")}}
                @else
                {{__("bonuses")}}
                @endif
                <br>
                <b>{{$Role->effect_amount}}
                  @if($Role->effect_is_precent>0)
                  %
                  @else
                  $
                  @endif
                </b>
              </td>
              @endif
              <td>
                <a href="{{route('Roles.edit',['Role' => $Role->id])}}" class="btn btn-secondary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-pen"></i>
                  </span>
                  <span class="text">{{__("Edit Feild")}}</span>
                </a>
                <a href="#" data-id="{{$Role->id}}" data-name="{{$Role->name}}" class="btn DeleteElement btn-danger btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-trash"></i>
                  </span>
                  <span class="text">{{__("Delete Feild")}}</span>
                </a>
                  <form style="display:inline;" id="DeleteElement{{$Role->id}}" action="{{route('Roles.destroy',['Role' => $Role->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                </form>


              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      @else
      {{__("there is not any roles yet please click add a role to add them.")}}
      @endif
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection
