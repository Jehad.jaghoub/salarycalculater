@extends('layouts.mainLayout')
@section('content')

          <!-- Begin Page Content -->
          <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">{{("Salary Calc")}} <sup>{{__("v 0.1")}}</sup></h1>
              <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
            </div>

            <!-- Content Row -->
            <div class="row">

              <!-- Earnings (Monthly) Card Example -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__("Average Salary")}} ({{__("Monthly")}})</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$AvgSalary}}</div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Earnings (Monthly) Card Example -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{__("Average Net Salary")}} ({{__("Monthly")}})</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$AvgNet}}</div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Earnings (Monthly) Card Example -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">{{("Average country taxes")}}</div>
                        <div class="row no-gutters align-items-center">
                          <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{round($avgTax)}}%</div>
                          </div>
                          <div class="col">
                            <div class="progress progress-sm mr-2">
                              <div class="progress-bar bg-info" role="progressbar" style="width: {{round($avgTax)}}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Pending Requests Card Example -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">{{__("Number of usage")}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$TotallogCount}}</div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Content Row -->

            <div class="row">
              @if (isset($Fromated))

              <div class="col-xl-12 col-md-12 mb-4">
                              <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                  <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">{{__("Your Net Salary")}}</div>
                                      <div class="h3 mb-0 font-weight-bold text-gray-800">{{$Fromated}}</div>
                                    </div>
                                    <div class="col-auto">
                                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
              @endif

              <!-- Area Chart -->
              <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{__("Calculate Salary")}}</h6>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <form method="post" action="{{Route('Calculate')}}">
                      @csrf
                      <div class="row">
                     <div class="form-group col-md-6 col-12">
                       <label for="EmployeeName">{{__("Employee Name")}}</label>
                       <input required minlength="3" type="text" class="form-control" name="EmployeeName" id="EmployeeName" aria-describedby="EmployeeName" placeholder="{{__("John Doe")}}">
                     </div>
                     <div class="form-group col-md-6 col-12">
                       <label for="Employee_Salary">{{__("Employee Salary")}}</label>
                       <input required min="700" type="number" class="form-control" name="Employee_Salary" id="Employee_Salary" placeholder="4000">
                     </div>
                     <div class="form-group col-md-8 col-12">
                       <label for="Country_Tax">{{__("Country Tax")}}</label>
                       <input required min="0" type="number" name="Country_Tax" class="form-control" id="Country_Tax" placeholder="20">
                       <small id="TaxHelp" class="form-text text-muted">Your Country Tax Precent Example 20%.</small>
                     </div>

                     @foreach($Fields as $Field)
                     @if($Field->is_checkbox)
                     <div style="margin-top:38px;" class="form-group col-6">
                      <div class="custom-control custom-switch">
                       <input type="checkbox" value="1" name="filedCalc_{{$Field->id}}" class="custom-control-input" id="filedCalc_{{$Field->id}}">
                       <label class="custom-control-label" for="filedCalc_{{$Field->id}}">{{$Field->name}}</label>
                      </div>
                    </div>
                     @else
                     <div class="form-group col-md-6 col-12">
                       <label for="filedCalc_{{$Field->id}}">{{$Field->name}}</label>
                       <input required type="number" class="form-control" name="filedCalc_{{$Field->id}}" id="filedCalc_{{$Field->id}}">
                     </div>
                     @endif
                     @endforeach
                     </div>
                     <button id="SubmitForm" type="submit" class="btn  btn-success btn-icon-split">
                       <span class="icon text-white-50">
                         <i class="fas fa-check"></i>
                       </span>
                        <span class="text">{{__("Calculate Salary")}}</span>
                    </button>
                   </form>
                  </div>
                </div>
              </div>

              <!-- Pie Chart -->
              <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{__("Recent Logs")}}</h6>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    @foreach($Recents as $log)
                    @php
                    $Element = json_decode($log->data);
                    @endphp

                    <a class="dropdown-item d-flex align-items-center" href="#">
                      <div class="dropdown-list-image mr-3">
                        <div class="status-indicator bg-success"></div>
                      </div>
                      <div class="font-weight-bold">
                        <div class="text-truncate">{{$Element->EmployeeName}}</div>
                        <div class="small text-gray-500">{{__("Salary")}}: {{$Element->Employee_Salary}}</div>
                        <div class="small text-gray-500">{{__("Net Salary")}}: {{$log->result}}</div>

                      </div>
                    </a>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>

            <!-- Content Row -->
            <div class="row">



              <div class="col-lg-12 mb-4">

                <!-- Illustrations -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">{{__("About Project")}}</h6>
                  </div>
                  <div class="card-body">
                    <div class="text-center">
                      <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="https://blackrockdigital.github.io/startbootstrap-sb-admin-2/img/undraw_posting_photo.svg" alt="">
                    </div>
                    <p>The <b>Salary Calc <sup>v 0.1</sub></b>  was created as interview task for the company  <a target="_blank" rel="nofollow" href="https://www.realforce.ch/">RealForce</a>,
                       By Jehad Jaghoub!<br> hopefully its understand well by the task sheet</p>
                       <h5 id="AppUsage">How it Works</h5>
                       <ul>
                         <li>
                           according to task the we need to have expandable system
                           to my understanding shall have dynamic Feilds which was achived for auth user to add them
                         </li>
                         <li>each Field have 1 or many roles according to it we can calcuate the salary so we need to make roles for the feilds</li>
                         <li>then we run calcualtion according to these values</li>
                       </ul>
                  </div>
                </div>

                <div id="Installation" class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">{{__("Installation Guide")}}</h6>
                  </div>
                  <div class="card-body">
                  <ul>
                    <li><code>$ git clone https://gitlab.com/Jehad.jaghoub/salarycalculater.git  salarycalculater</code></li>
                    <li><code>$ cd  salarycalculater</code></li>
                    <li><code>$ composer install</code></li>
                    <li><code>$ nano .env  </code> change DB_DATABASE=/absolute/path/salary.sqlite <br>
                      eg. Homestead Laravel (ubuntu) Vagrant <code>DB_DATABASE=/home/vagrant/Code/salaryCalculater/database/salary.sqlite
</code>


                    <li>Database is included as Sqlite file, you can delete it and create database on MySQL, PostgreSQL, SQLite, SQL Server
                      to delete it and have fresh sqlite db<br>
                      <code>$ rm database/salary.sqlite</code><BR>
                    <code>$ touch database/salary.sqlite</code><br>
                    <code>$ php artisan migrate</code><br>
                    add default admin to db ,roles and feilds
                    <code>$ php artisan db:seed </code><br>
                    if you need to use other database type example mysql  <br>
                    1- you need to create the database on you server/pc <br>
                    2- make the correct connection inside file .env<br>
                    3-<code>$ php artisan migrate</code><br>
                    add default admin to db ,roles and feilds
                    <code>$ php artisan db:seed </code><br>
                    </li>
                    <li>if you need to modifiy scss or javascripts code  <code>$ npm i </code>
<br>
watching <code>npm run watch</code>
<br>
Deveopment <code>npm run dev</code>
<br>
production <code>npm run prod</code>

                    </li>
                    <li>to serve the app run   <code>$ php artisan serve </code></li>
                  </li>
                  </ul>


                  <h3>Testing Unit</h3>

                  test files exist on
                  <code>tests</code>
                  <br>
                  to run all app test
                  <code>vendor/bin/phpunit</code><br>
                  to run exact app test eg. test_bob_get_salary
                  <code>vendor/bin/phpunit --filter test_bob_get_salary</code>


                  </div>
                </div>

                <!-- Approach -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">{{__("Imporant Info")}}</h6>
                  </div>
                  <div class="card-body">
                  <ul>
                    <li>Sql perfomance was not consider as it's not main part of the task</li>
                    <li>the project could be done more professional using Ajax calls or useing modren javascript frameworks eg. Vue , Angular , React
                      but as the job title is backend developer it mostly done there
                    </li>
                    <li>for faster results Laravel frameworks was used!</li>
                    <li>the project can work on 4 database soruce MySQL, PostgreSQL, SQLite, SQL Server , i hardcoded SQLite to make it work directly without sql server service
                    any way can be changed on file
                    <code> 'config/database.php' </code>
                    <br>
                    <code>'default' => 'sqlite',</code>
                    to make it according to ENV
                  </li>
                  </ul>
                  </div>
                </div>

              </div>
            </div>

          </div>
          <!-- /.container-fluid -->
@endsection
