<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use \App\User;
use \App\userField;

class FieldsTest extends TestCase
{
    use RefreshDatabase;
    /*
    * Testing if Browse Feilds is Disabled from Users
    */
    public function test_fields_browse_only_auth()
    {
        $response = $this->get('/Fileds');
        $response->assertRedirect('/login');
    }

    public function test_fields_add_only_auth()
    {
        $response = $this->get('/Fileds/create');
        $response->assertRedirect('/login');
    }


    public function test_fields_edit_only_auth()
    {
        $response = $this->get('/Fileds/1/edit');
        $response->assertRedirect('/login');
    }

    public function test_fields_auth_can_browse()
    {
        $this->actingAs(factory(User::class)->create());
        $response =   $this->get('/Fileds');
        $response->assertOk();
    }

    public function test_add_filed_to_db()
    {
        $this->actingAs(factory(User::class)->create());
        $response =   $this->post('/Fileds', [
          'FeildName'=>'Test',
          'FeildOrder'=>1,
          'is_checkbox'=>1,
        ]);
        $this->assertCount(1, userField::all());
    }
}
