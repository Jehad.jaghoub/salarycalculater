<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use \App\User;
use \App\Role;
use \App\userField;
use \App\Userlog;

class MainTest extends TestCase
{
    use RefreshDatabase;
    /*
    * Testing if Browse Feilds is Disabled from Users
    */
    public function test_home_browse_not_auth()
    {
        $response = $this->get('/');
        $response->assertOk();
    }


    public function test_home_auth_can_browse()
    {
        $this->actingAs(factory(User::class)->create());
        $response =   $this->get('/');
        $response->assertOk();
    }

    public function test_alice_get_salary()
    {
        $this->actingAs(factory(User::class)->create());
        // add the main 3 fileds
        factory(userField::class)->create([
          'name'=>'Age',
          'order'=>1,
          'is_checkbox'=>0,
        ]);
        factory(userField::class)->create([
          'name'=>'Kids',
          'order'=>2,
          'is_checkbox'=>0,
        ]);
        factory(userField::class)->create([
          'name'=>'car',
          'order'=>3,
          'is_checkbox'=>1,
        ]);
        // add 3 main roles
        factory(Role::class)->create([
          'name'=>'Age',
          'userfield_id'=>'1',
          'will_effect'=>'0',
          'effect_type'=>'0',
          'effect_amount'=>'7',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'50'
        ]);

        factory(Role::class)->create([
          'name'=>'Kids',
          'userfield_id'=>'2',
          'will_effect'=>'1',
          'effect_type'=>'0',
          'effect_amount'=>'2',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'2'
        ]);

        factory(Role::class)->create([
          'name'=>'car',
          'userfield_id'=>'3',
          'will_effect'=>'0',
          'effect_type'=>'1',
          'effect_amount'=>'500',
          'effect_is_precent'=>'0',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'0'
        ]);

        // check if we have roles and fields
        $this->assertCount(3, userField::all());
        $this->assertCount(3, Role::all());

        $response =   $this->post('/', [
          'EmployeeName'=>'Alice',
          'Employee_Salary'=>6000, // salary
          'Country_Tax'=>20, // Country_Tax
          'filedCalc_1'=>26, // Age
          'filedCalc_2'=>2, // Kids
          'filedCalc_3'=>0, // using car
        ]);
        $response->assertSee('<div class="h3 mb-0 font-weight-bold text-gray-800">$4800.00</div>');
        $this->assertCount(1, Userlog::all());



        // now roles are added exactly as task .
        // we need to check if results appear
    }

    public function test_bob_get_salary()
    {
        $this->actingAs(factory(User::class)->create());
        // add the main 3 fileds
        factory(userField::class)->create([
          'name'=>'Age',
          'order'=>1,
          'is_checkbox'=>0,
        ]);
        factory(userField::class)->create([
          'name'=>'Kids',
          'order'=>2,
          'is_checkbox'=>0,
        ]);
        factory(userField::class)->create([
          'name'=>'car',
          'order'=>3,
          'is_checkbox'=>1,
        ]);
        // add 3 main roles
        factory(Role::class)->create([
          'name'=>'Age',
          'userfield_id'=>'1',
          'will_effect'=>'0',
          'effect_type'=>'0',
          'effect_amount'=>'7',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'50'
        ]);

        factory(Role::class)->create([
          'name'=>'Kids',
          'userfield_id'=>'2',
          'will_effect'=>'1',
          'effect_type'=>'0',
          'effect_amount'=>'2',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'2'
        ]);

        factory(Role::class)->create([
          'name'=>'car',
          'userfield_id'=>'3',
          'will_effect'=>'0',
          'effect_type'=>'1',
          'effect_amount'=>'500',
          'effect_is_precent'=>'0',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'0'
        ]);

        // check if we have roles and fields
        $this->assertCount(3, userField::all());
        $this->assertCount(3, Role::all());

        $response =   $this->post('/', [
          'EmployeeName'=>'Bob',
          'Employee_Salary'=>4000, // salary
          'Country_Tax'=>20, // Country_Tax
          'filedCalc_1'=>52, // Age
          'filedCalc_2'=>0, // Kids
          'filedCalc_3'=>1, // using car
        ]);
        $response->assertSee('<div class="h3 mb-0 font-weight-bold text-gray-800">$2924.00</div>');
        $this->assertCount(1, Userlog::all());



        // now roles are added exactly as task .
        // we need to check if results appear
    }


    public function test_charlie_get_salary()
    {
        $this->actingAs(factory(User::class)->create());
        // add the main 3 fileds
        factory(userField::class)->create([
          'name'=>'Age',
          'order'=>1,
          'is_checkbox'=>0,
        ]);
        factory(userField::class)->create([
          'name'=>'Kids',
          'order'=>2,
          'is_checkbox'=>0,
        ]);
        factory(userField::class)->create([
          'name'=>'car',
          'order'=>3,
          'is_checkbox'=>1,
        ]);
        // add 3 main roles
        factory(Role::class)->create([
          'name'=>'Age',
          'userfield_id'=>'1',
          'will_effect'=>'0',
          'effect_type'=>'0',
          'effect_amount'=>'7',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'50'
        ]);

        factory(Role::class)->create([
          'name'=>'Kids',
          'userfield_id'=>'2',
          'will_effect'=>'1',
          'effect_type'=>'0',
          'effect_amount'=>'2',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'2'
        ]);

        factory(Role::class)->create([
          'name'=>'car',
          'userfield_id'=>'3',
          'will_effect'=>'0',
          'effect_type'=>'1',
          'effect_amount'=>'500',
          'effect_is_precent'=>'0',
          'role_meet_on'=>'0',
          'role_on_meet_amount'=>'0'
        ]);

        // check if we have roles and fields
        $this->assertCount(3, userField::all());
        $this->assertCount(3, Role::all());

        $response =   $this->post('/', [
          'EmployeeName'=>'Charlie',
          'Employee_Salary'=>5000, // salary
          'Country_Tax'=>20, // Country_Tax
          'filedCalc_1'=>36, // Age
          'filedCalc_2'=>3, // Kids
          'filedCalc_3'=>1, // using car
        ]);
        $response->assertSee('<div class="h3 mb-0 font-weight-bold text-gray-800">$3600.00</div>');
        $this->assertCount(1, Userlog::all());

        // now roles are added exactly as task .
        // we need to check if results appear
    }
}
