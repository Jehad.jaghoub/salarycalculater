<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use \App\User;
use \App\Role;

class RoleTest extends TestCase
{
    use RefreshDatabase;
    /*
    * Testing if Browse Feilds is Disabled from Users
    */
    public function test_roles_browse_only_auth()
    {
        $response = $this->get('/Roles');
        $response->assertRedirect('/login');
    }

    public function test_roles_add_only_auth()
    {
        $response = $this->get('/Roles/create');
        $response->assertRedirect('/login');
    }


    public function test_roles_edit_only_auth()
    {
        $response = $this->get('/Roles/1/edit');
        $response->assertRedirect('/login');
    }

    public function test_roles_auth_can_browse()
    {
        $this->actingAs(factory(User::class)->create());
        $response =   $this->get('/Roles');
        $response->assertOk();
    }

    public function test_add_roles_to_db()
    {
        $this->actingAs(factory(User::class)->create());
        // add user feild first
        $response =   $this->post('/Fileds', [
          'FeildName'=>'Test',
          'FeildOrder'=>1,
          'is_checkbox'=>1,
        ]);
        $response =   $this->post('/Roles', [
          'RoleName'=>'test',
          'userfield_id'=>'1',
          'will_effect'=>'0',
          'effect_type'=>'1',
          'effect_amount'=>'0',
          'effect_is_precent'=>'1',
          'role_meet_on'=>'2',
          'role_on_meet_amount'=>'1'
        ]);
        $this->assertCount(1, Role::all());
    }
}
